INTERFAZ BÁSICA REACT

Descripción

Se desarrolló una interfaz básica y sencilla sin control de backend, mostrando y observando las capacidades y limitaciones de dicho entorno de desarrollo para una página web. Este proyecto no se basa en algo rígido y es plenamente de prueba y estudio, mostrando cualquier funcionamiento y acción del react en torno al objetivo planteado por el proyecto.

El proyecto se basó en una cartelera de información sobre los eventos de la Universidad Católica Andrés Bello.

Funcionalidad

La cartelera se manipula de forma interactiva, sin tiempos de carga, mostrando los eventos en primera plana pero además permitiendo observar los mismos en el calendario. Se necesitó de un botón de actualización para observar estos.

ERRORES: la actualización no es inmediata al cambiar de categoría.


Author: HideScript, Yorfrank Bastidas.