import React, { Component } from 'react';
import '../App.css';
import prueba from "../mgs.jpg";
import $ from 'jquery';

class Foot extends Component{

  render(){
    return (
      <div className="Foot bg-dark">
        <div aria-live="polite" aria-atomic="true" className="d-flex justify-content-center align-items-center">
          <div className="toast" role="alert" aria-live="assertive" aria-atomic="true">
            <div className="toast-header">
              <img src="..." className="rounded mr-2" alt="..."/>
              <strong className="mr-auto">Bootstrap</strong>
              <small>11 mins ago</small>
              <button type="button" className="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="toast-body">
              Presiona actualizar al pasar de mes para observar los eventos especificos en ese tramo de tiempo.
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Foot
