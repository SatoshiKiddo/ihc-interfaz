import React, { Component } from 'react';
import logo from '../logo.png';
import '../App.css';

class Header extends Component {
  render() {
    return (
      <header className="Header">
        <img src={logo} className="App-logo" alt="Logo Cartelera Ucab"/>
      </header>
    );
  }
}

export default Header
