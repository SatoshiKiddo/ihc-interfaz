import React, { Component } from 'react';
import '../App.css';
import Despliegue from '../despliegueIcon.png';
import logo from '../logo.svg';
import $ from 'jquery';
import {general, ing_informatica} from '../Eventos/General.json';

class Navigation extends Component {
  constructor(){
    super();
    this.state={
      categoria:"General",
      eventos:general
    }
    this.posicionCartelera = this.posicionCartelera.bind(this);
  }

  posicionCartelera(e){
    const nombre= e.target.id;
    switch (nombre){
        case "Ing Informatica": this.setState({
          categoria:nombre,
          eventos:ing_informatica
            });
            break;
        case "General": this.setState({
          categoria:nombre,
          eventos:general
          });
          break;
        default: this.setState({
          categoria:nombre,
          eventos:[]
        })
        break;
    }
    this.props.onCambioDeCategoria(this.state);
    document.getElementById('Posicion').innerHTML = nombre;
  }

  render() {
    return(
      <div className="Navigation shadow-lg" id="barra">
          <nav className="navbar navbar-default bg-dark shadow-lg">
            <div className="container-fluid shadow-lg">
              <div className="navbar-header">
                <a className="navbar-brand" href="#">
                  <img src={logo} alt="icon" className="App-logo2"/>
                </a>
                <button type="button" className="navDesp btn-sm btn navbar-toggle" data-toggle="collapse" data-target=".Menu">
                  <span class="sr-only">Menu</span>
                  <div class="icon-bar"></div>
                  <div class="icon-bar"></div>
                  <div class="icon-bar"></div>
                </button>
              </div>
              <div className="collapse navbar-collapse Menu" id="dashboard">
                <div className="btn-group">
                  <button type="button" className="btn btn-secondary btn-lg btn-dark dropdown-toggle" data-toggle="dropdown">
                    Ingenieria
                  </button>
                  <div className="dropdown-menu">
                    <a className="dropdown-item categoria" href="#" id="Ing Informatica" onClick={this.posicionCartelera.bind()}>Ing. Informatica</a>
                    <a className="dropdown-item categoria" href="#" id="Ing Industrial" onClick={this.posicionCartelera.bind()}>Ing. Industrial</a>
                    <a className="dropdown-item categoria" href="#" id="Ing Civil" onClick={this.posicionCartelera.bind()}>Ing Civil</a>
                    <a className="dropdown-item categoria" href="#" id="Ing Telecomunicaciones" onClick={this.posicionCartelera.bind()}>Ing. Telecomunicaciones</a>
                  </div>
                </div>
                <div className="btn-group">
                  <button type="button" className="btn btn-secondary btn-lg btn-dark dropdown-toggle" data-toggle="dropdown">
                    Humanidades
                  </button>
                </div>
                <div className="btn-group">
                  <button type="button" className="btn btn-secondary btn-lg btn-dark dropdown-toggle" data-toggle="dropdown">
                    General
                    </button>
                    <div className="dropdown-menu">
                      <a className="dropdown-item categoria" href="#" id="Centro Cultural" onClick={this.posicionCartelera.bind()}>Centro Cultural</a>
                      <a className="dropdown-item categoria" href="#" id="General" onClick={this.posicionCartelera.bind()}>General</a>
                    </div>
                </div>
              </div>
            </div>
          </nav>
          <div class="alert alert-dark collapse Menu shadow-lg" role="alert">
            <span className="badge badge-danger shadow-lg"> Cartelera <h5 id="Posicion">General</h5></span>
          </div>
      </div>
    );
  }
}

export default Navigation;
