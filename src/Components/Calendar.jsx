import React from "react";
import dateFns from "date-fns";
import '../App.css';
import './Calendar.css';

class Calendar extends React.Component {
  state = {
    currentMonth: new Date(),
    selectedDate: new Date()
  };

  placeEvents(){
    const {currentMonth} = this.state;
    const monthStart = dateFns.startOfMonth(currentMonth);
    const monthEnd = dateFns.endOfMonth(monthStart);
    const startDate = dateFns.startOfWeek(monthStart);
    const endDate = dateFns.endOfWeek(monthEnd);
    let day = startDate;

    const events = this.props.eventos;
    var init = 0;
    var max = events.length;


    while (day <= endDate) {
      for (let i = 0; i < 7; i++) {
        var formattedDay = dateFns.format(day, 'DD/MM/YYYY');
        while (init < max){
          if( formattedDay ===  events[init].dia + "/"+ events[init].mes + "/" + events[init].ano ){
            var cell = document.getElementsByName("eventHere");
            var init2= 0;
            var max2 = cell.length;
            while(init2 < max2){
              if(formattedDay === cell[init2].innerHTML){
                cell[init2].hidden = false;
                cell[init2].innerHTML = events[init].titulo;
              }
              init2++;
            }
            init2 =0;
          }
          init++;
        }
        init =0;
        day = dateFns.addDays(day, 1);
      }
    }
  }

  renderHeader() {
    const dateFormat = "MMMM YYYY";

    return (
      <div className="header row flex-middle">
        <div className="col col-start">
          <div className="icon" onClick={this.prevMonth}>
            chevron_left
          </div>
        </div>
        <div className="col col-center">
          <span>{dateFns.format(this.state.currentMonth, dateFormat)}</span>
          <br/>
          <span class= "d-inline-block" data-toggle="tooltip" data-placement="right" title="Presiona para observar los eventos del mes">
            <button type="button" class="btn btn-primary btn-lg btn-dark"
              style={{padding : ".2em", marginTop: ".5em"}}
              onClick={this.placeEvents.bind(this)}> Actualizar </button>
          </span>
        </div>
        <div className="col col-end" onClick={this.nextMonth}>
          <div className="icon">chevron_right</div>
        </div>
      </div>
    );
  }

  renderDays() {
    const dateFormat = "dddd";
    const days = [];

    let startDate = dateFns.startOfWeek(this.state.currentMonth);

    for (let i = 0; i < 7; i++) {
      days.push(
        <div className="col col-center" key={i}>
          {dateFns.format(dateFns.addDays(startDate, i), dateFormat)}
        </div>
      );
    }

    return <div className="days row">{days}</div>;
  }

  renderCells() {
    const { currentMonth, selectedDate } = this.state;
    const monthStart = dateFns.startOfMonth(currentMonth);
    const monthEnd = dateFns.endOfMonth(monthStart);
    const startDate = dateFns.startOfWeek(monthStart);
    const endDate = dateFns.endOfWeek(monthEnd);
    const dateFormat = "D";
    const rows = [];
    let days = [];
    let day = startDate;
    let formattedDate = "";
    while (day <= endDate) {
      for (let i = 0; i < 7; i++) {
        formattedDate = dateFns.format(day, dateFormat);
        const cloneDay = day;
        days.push(
          <div
            className={`col cell ${
              !dateFns.isSameMonth(day, monthStart)
                ? "disabled"
                : dateFns.isSameDay(day, selectedDate) ? "selected" : ""
            }`}
            id={`${
              !dateFns.isSameMonth(day, monthStart)
                ? "disabled"
                : dateFns.isSameDay(day, selectedDate) ? "clicked" : " "
            }`}
            key={day}
            onClick={
              () => this.onDateClick(dateFns.parse(cloneDay))
            }
          >
            <span hidden name ="eventHere" >{dateFns.format(day, 'DD/MM/YYYY')}</span>
            <span className="number">{formattedDate}</span>
            <span className="bg">{formattedDate}</span>
          </div>
        );
        day = dateFns.addDays(day, 1);
      }
      rows.push(
        <div className="row" key={day}>
          {days}
        </div>
      );
      days = [];
    }
    this.placeEvents();
    return <div className="body">{rows}</div>;
  }

  onDateClick = (day) => {
    this.setState({
      selectedDate: day
    });
  };

  nextMonth = () => {
    this.setState({
      currentMonth: dateFns.addMonths(this.state.currentMonth, 1)
    });
  };

  prevMonth = () => {
    this.setState({
      currentMonth: dateFns.subMonths(this.state.currentMonth, 1)
    });
  };

  render() {
    return (
      <div className="calendar">
        {this.renderHeader()}
        {this.renderDays()}
        {this.renderCells()}
      </div>
    );
  }
  componentDidMount(){
    this.placeEvents();
  }
}

export default Calendar;
