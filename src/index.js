import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import $ from 'jquery';

ReactDOM.render(<App />, document.getElementById('root'));

//Codigo de la barra de navegacionfija.
$(window).scroll(function() {
    posicionarMenu();
});

//Interacciones dentro de la pagina web.
$('.card').hover(
  function(){
  $('img.Fondo').css({"filter":"blur(6px)"});
  },
  function(){
  $('img.Fondo').css({"filter":"none"});
  }
);

$('.thumbnail').hover(
  function(){
  $('img.Fondo').css({"filter":"blur(6px)"});
  },
  function(){
  $('img.Fondo').css({"filter":"none"});
  }
);


function posicionarMenu() {
    var altura_del_header = $('.Header').outerHeight(true);
    var altura_del_menu = $('.Cartelera').outerHeight(true);

    if ($(window).scrollTop() >= altura_del_header){
        $('#barra').addClass('navbar-fixed');
    } else {
        $('#barra').removeClass('navbar-fixed');
    }
}

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
