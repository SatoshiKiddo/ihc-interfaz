import React, { Component } from 'react';
import logo from './logo.png';
import './App.css';
import './Components/Calendar.css'
import Header from "./Components/Header";
import Navigation from "./Components/Navigation";
import Foot from "./Components/Foot";
import Calendar from "./Components/Calendar";
import $ from 'jquery';
import prueba from "./mgs.jpg";
import {general} from './Eventos/General.json';
import Img from 'react-image';
import {categoria, evento} from './estado.json';

class App extends Component {
  constructor(){
    super();
    this.state={categoria:"General",
    eventos:general};
    this.cambioDecategoria=this.cambioDecategoria.bind(this);
  }

  cambioDecategoria(state){
    this.setState(state);
  }

  render() {
    var activo="carousel-item active";
    var inactivo="carousel-item";
    var estado=activo;
    var eventosMostrar;
    var listaEventos;
    var imagen;
    if(this.state.eventos.length != 0){
      eventosMostrar = this.state.eventos.map((evento,i) => {
      if (i != 0){
        estado=inactivo
      }
      return (
        <div className={estado}>
          <img src={evento.imagen} alt="..." className="d-block w-100 Fondo"/>
            <div className="card col-md-5 overflow-auto">
              <div className="card-body blur overflow-auto">
                <h5 className="card-title">{evento.titulo}</h5>
                <h6 className="card-subtitle mb-2 text-muted">{evento.subtitulo}</h6>
                <p className="card-text">{evento.descripcion}</p>
              </div>
              <ul className="list-group list-group-flush">
                <li className="list-group-item">
                  <span className="fecha badge badge-secondary"><strong>Fecha:</strong> {evento.dia}/{evento.mes}/{evento.ano}</span>
                </li>
              </ul>
              <ul className="list-group list-group-flush">
                <li className="list-group-item">
                  <a href={evento.link} className="badge badge-primary">
                    Mas info
                    </a>
                </li>
              </ul>
            </div>
            <img src={evento.imagen} className="rounded thumbnail"/>
          </div>
        )
      })
    }
    else{
      eventosMostrar = (
          <div className={estado}>
            <img src="https://pbs.twimg.com/media/DqXZ_VCWkAEDN_d.jpg" className="d-block w-100 Fondo bg-dark" alt="No hay eventos aca"/>
              <div className="card col-md-5 overflow-auto vacio">
                <div className="card-body blur overflow-auto">
                  <h5 className="card-title">No hay proximos eventos</h5>
                  <p className="card-text">Lo sentimos, pero en esta seccion no hay proximos eventos cercanos por ahora.</p>
                </div>
              </div>
          </div>
        );
      }

    activo="active";
    inactivo="";
    estado=activo;
    if (this.state.eventos.length != 0){
    listaEventos = this.state.eventos.map((evento,i) => {
      if (i != 0){
        estado=inactivo
      }
      return (
        <li data-target="#carouselExampleIndicators" data-slide-to={i} className={estado}></li>
      )
    })
    }
    else{
      listaEventos = function () {
        return (
          <li data-target="#carouselExampleIndicators" data-slide-to="1" className={estado}></li>
        )
      }
    }

    return (
      <div className="App">
        <Header/>
        <Navigation onCambioDeCategoria={this.cambioDecategoria}/>
        <div className="Cartelera">
          <div id="carouselExampleIndicators" className="carousel slide" data-ride="carousel">
            <ol className="carousel-indicators">
              {listaEventos}
            </ol>
            <div className="carousel-inner">
              {eventosMostrar}
            </div>
            <a className="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
              <span className="carousel-control-prev-icon" aria-hidden="true"></span>
              <span className="sr-only">Previous</span>
            </a>
            <a className="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
              <span className="carousel-control-next-icon" aria-hidden="true"></span>
              <span className="sr-only">Next</span>
            </a>
          </div>
        </div>
        <Foot/>
        <Calendar eventos = {this.state.eventos}/>
        <div className="Final">
          <div className="prom" id="simbol-ucab">
            <img src="http://www.igez.edu.ve/wp-content/uploads/2014/07/ucab.png?189db0&189db0" className="img-fluid" alt="Universidad Catolica Andres Bello"/>
          </div>
          <div className="prom" id="simbol-ucab">
            <img src="https://vida.ucab.edu.ve/wp-content/uploads/sites/2/2017/04/IGENIERIA.png" className="img-fluid" alt="Universidad Catolica Andres Bello"/>
          </div>
          <div className="prom">
          epale
          </div>
          <div className="prom">
          epale
          </div>
        </div>
      </div>
    )
  }
}



export default App;
