import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});

posicionarMenu();

$(window).scroll(function() {
posicionarMenu();
});

function posicionarMenu() {
var altura_del_header = $('.Header').outerHeight(true);
var altura_del_menu = $('#barra').outerHeight(true);

if ($(window).scrollTop() >= altura_del_header){
    $('#barra').addClass('fixed');
} else {
    $('#barra').removeClass('fixed');
}
